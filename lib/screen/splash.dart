import 'package:flutter/material.dart';
import 'package:rental_app/localization/app_localizations.dart';
import 'package:rental_app/utils/my_constants.dart';
import 'package:rental_app/utils/share_preference.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'home.dart';
import 'login.dart';


class MySplash extends StatefulWidget {
  @override
  _MySplashState createState() => new _MySplashState();
}

class _MySplashState extends State<MySplash> {
  ///Dynamic instance for screen redirect
  dynamic redirectScreen = null;

  @override
  void initState() {
    super.initState();

    getCredential();

    //Delay for 2 seconds then redirect to Login or Main screen
    Future.delayed(Duration(seconds: 2), () {
      Navigator.pushReplacement(
          context,
          MaterialPageRoute(
            builder: (context) => redirectScreen,
          ));
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.red,
      body: Container(
        alignment: Alignment.center,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text(
              AppLocalizations.of(context).translate('splash_msg'),
              textAlign: TextAlign.center,
              style: new TextStyle(
                  fontSize: 20.0,
                  color: Colors.white,
                  fontWeight: FontWeight.bold),
            )
          ],
        ),
      ),
    );
  }

  ///Check is user login or not for screen redirect
  getCredential() {
    MySharePreference().getBoolInPref(MyConstants.PREF_KEY_ISLOGIN).then((value) {
      if(value){
        redirectScreen = MyHomePage(title: 'Piyush Flutter Home screen');
      }else{
        redirectScreen = MyLogin(title: AppLocalizations.of(context).translate('label_login'));
      }
    });
  }
}