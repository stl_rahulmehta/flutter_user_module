import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:rental_app/localization/app_localizations.dart';
import 'package:rental_app/models/main_category_pojo.dart';
import 'package:rental_app/screen/properties/product_list.dart';

class SelectSubCategory extends StatefulWidget {
  final String title;
  List<Datum> subCategories;

  SelectSubCategory({Key key, this.title, this.subCategories})
      : super(key: key);

  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return new _SelectSubCategoryStateFul();
  }
}

class _SelectSubCategoryStateFul extends State<SelectSubCategory> {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      appBar: AppBar(
        title: Text(
            AppLocalizations.of(context).translate("label_select_category")),
      ),
      body: widget.subCategories == null
          ? new Center(
              child: new CircularProgressIndicator(),
            )
          : loadCategoryGridData(widget.subCategories),
    );
  }

  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    // Clean up the controller when the Widget is disposed
    super.dispose();
  }

  ///Load and display Sub category in list
  ListView loadCategoryGridData(List<Datum> _getSubcategores) {
    return new ListView.builder(
      itemCount: _getSubcategores.length,
      itemBuilder: (context, position) {
        return Card(
          child: InkWell(
              onTap: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => ProductsList(
                              title: _getSubcategores[position]
                                  .categoryName
                                  .toString(),
                              categoryId:
                                  _getSubcategores[position].id.toString(),
                              categoryType: widget.title,
                            )));
              },
              child: Padding(
                padding: const EdgeInsets.all(16.0),
                child:
                    Text(_getSubcategores[position].categoryName, maxLines: 1),
              )),
        );
      },
    );
  }
}
