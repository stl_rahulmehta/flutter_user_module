import 'package:flutter/material.dart';
import 'package:rental_app/localization/app_localizations.dart';
import 'package:rental_app/screen/login.dart';
import 'package:rental_app/utils/my_constants.dart';
import 'package:rental_app/utils/my_utils.dart';
import 'package:rental_app/utils/share_preference.dart';

class MyAccountFragment extends StatefulWidget {
  final String title;

  const MyAccountFragment({Key key, this.title}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return new _MyAccountFragmentStateFul();
  }
}

class _MyAccountFragmentStateFul extends State<MyAccountFragment> {
  //Handle email input controller
  final textEmailController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      appBar: AppBar(
        title: Text(AppLocalizations.of(context).translate("label_my_account")),
        automaticallyImplyLeading: false,
      ),
      body: Center(
        child: SingleChildScrollView(
          child: Container(
            margin: const EdgeInsets.only(
                left: MyConstants.layout_margin,
                right: MyConstants.layout_margin),
            child: Column(
              children: <Widget>[
                //Todo for manage space bewtween control
                new Container(
                  margin: new EdgeInsets.fromLTRB(
                      0.0,
                      MyConstants.vertical_control_space,
                      0.0,
                      MyConstants.vertical_control_space),
                ),
                RaisedButton(
                  onPressed: () {
                    _submitClickValidation();
                  },
                  child: Text(
                      AppLocalizations.of(context).translate("label_logout")),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }

  ///Validate when user will press Submit button
  void _submitClickValidation() {
    setState(() {
      MySharePreference().clearAllPref();
      Navigator.pushAndRemoveUntil(
        context,
        MaterialPageRoute(builder: (context) => MyLogin(title: AppLocalizations.of(context).translate('label_login'))),
            (Route<dynamic> route) => false,
      );
    });
  }

  @override
  void dispose() {
    // Clean up the controller when the Widget is disposed
    textEmailController.dispose();
    super.dispose();
  }
}
