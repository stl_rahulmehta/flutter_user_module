import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:rental_app/localization/app_localizations.dart';
import 'package:rental_app/models/main_category_pojo.dart';
import 'package:rental_app/presenter/getcategory_presenter.dart';
import 'package:rental_app/screen/appbar_common.dart';
import 'package:rental_app/screen/category/select_sub_category.dart';
import 'package:rental_app/screen/location/all_state_list.dart';
import 'package:rental_app/utils/my_constants.dart';
import 'package:rental_app/utils/my_utils.dart';
import 'package:rental_app/utils/share_preference.dart';

class ExploreFragment extends StatefulWidget {
  final String title;

  const ExploreFragment({Key key, this.title}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return new _ExploreFragmentStateFul();
  }
}

class _ExploreFragmentStateFul extends State<ExploreFragment>
    implements GetCategoryContract {
  GetCategoryPresenter _categoryPresenter;
  GETCategoryPojo getCategoryPojo;
  String selectedLocation = "";

  @override
  void initState() {
    // Check for internet
    MyUtils().check().then((intenet) {
      if (intenet != null && intenet) {
        _categoryPresenter = new GetCategoryPresenter(this);
        _categoryPresenter.doGetCategory(context);
      } else {
        MyUtils().toastdisplay(
            AppLocalizations.of(context).translate('msg_no_internet'));
      }
    });

    _getSelectedLocation();

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      appBar: CustomAppBar(
        appBar: AppBar(
          title: Text(selectedLocation),
        ),
        onTap: () {
         _navigateToNextScreen(context);
        },
      ),
      body: getCategoryPojo == null
          ? new Center(
              child: new CircularProgressIndicator(),
            )
          : loadCategoryGridData(getCategoryPojo),
    );
  }

  @override
  void dispose() {
    super.dispose();
  }

  /// Load category data from api
  GridView loadCategoryGridData(GETCategoryPojo categoryData) {
    return new GridView.builder(
        itemCount: categoryData.data.length,
        gridDelegate:
            new SliverGridDelegateWithFixedCrossAxisCount(crossAxisCount: 3),
        itemBuilder: (BuildContext context, int index) {
          return new GestureDetector(
            child: new Card(
              elevation: 5.0,
              child: new Column(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: <Widget>[
                  _widgetCategoryImage(categoryData.data[index].categoryName),
                  Text(
                    categoryData.data[index].categoryName,
                    maxLines: 1,
                    textAlign: TextAlign.center,
                  ),
                ],
              ),
            ),
            onTap: () {
              if (!categoryData.data[index].subCategories.isEmpty) {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => SelectSubCategory(
                              title: categoryData.data[index].categoryName,
                              subCategories:
                                  categoryData.data[index].subCategories,
                            )));
              }
            },
          );
        });
  }

  /// Set image from assets for Category
  Image _widgetCategoryImage(categoryType) {
    String imagePath = "";

    if (categoryType == MyConstants.CATEGORY_TYPE_PROPERTIES) {
      imagePath = 'graphics/properties.png';
    } else if (categoryType == MyConstants.CATEGORY_TYPE_FURNITURE) {
      imagePath = 'graphics/furniture.png';
    } else if (categoryType == MyConstants.CATEGORY_TYPE_BOOKS) {
      imagePath = 'graphics/books.png';
    } else if (categoryType == MyConstants.CATEGORY_TYPE_DECORATION) {
      imagePath = 'graphics/decoration.png';
    } else if (categoryType == MyConstants.CATEGORY_TYPE_ELECTRONICS) {
      imagePath = 'graphics/electronics.png';
    } else if (categoryType == MyConstants.CATEGORY_TYPE_ORNAMENTS) {
      imagePath = 'graphics/ornament.png';
    } else if (categoryType == MyConstants.CATEGORY_TYPE_TOYS) {
      imagePath = 'graphics/toys.png';
    } else if (categoryType == MyConstants.CATEGORY_TYPE_FASHION) {
      imagePath = 'graphics/fashion.png';
    } else {
      imagePath = 'graphics/vehicles.png';
    }

    return new Image.asset(
      imagePath,
      height: MyConstants.image_category_rounded,
      width: MyConstants.image_category_rounded,
    );
  }

  /// Handle api calling error
  @override
  void onGetCategoryError(String errorTxt) {
    MyUtils().toastdisplay(errorTxt);
  }

  /// Handle api calling success flow
  @override
  void onGetCategorySuccess(GETCategoryPojo pojoData) {
    if (this.mounted) {
      getCategoryPojo = pojoData;
      setState(() {
        print('UI Updated');
      });
    }
  }

  /// Get selected location from preference
  _getSelectedLocation() {
    MySharePreference()
        .getStringInPref(MyConstants.PREF_SELECTED_LOCATION)
        .then((value) {
      setState(() {
        if (value.isEmpty) {
          selectedLocation =
              AppLocalizations.of(context).translate("label_location");
        } else {
          selectedLocation = value;
        }
      });
    });
  }

  /// Redirect to new screen
  _navigateToNextScreen(BuildContext context) async {
    // Navigator.push returns a Future that completes after calling
    // Navigator.pop on the Selection Screen.

    var result = null;

    result = await  Navigator.push(
        context, MaterialPageRoute(builder: (context) => GetAllStates()));

    if (result != null) {
      _getSelectedLocation();
    }
  }
}
