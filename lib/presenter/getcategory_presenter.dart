import 'package:flutter/cupertino.dart';
import 'package:rental_app/models/main_category_pojo.dart';
import 'package:rental_app/network/rest_ds.dart';

abstract class GetCategoryContract {
  void onGetCategorySuccess(GETCategoryPojo user);

  void onGetCategoryError(String errorTxt);
}

//GetCategoryPresenter for call and handle api flow
class GetCategoryPresenter {
  GetCategoryContract _view;
  RestDatasource api = new RestDatasource();

  GetCategoryPresenter(this._view);

  doGetCategory(BuildContext getContext) async {
    try {
      await api.getCategory(getContext).then((GETCategoryPojo pojoData) {
        _view.onGetCategorySuccess(pojoData);
      });
    } catch (error) {
      print('Error occured: $error');
      _view.onGetCategoryError(error.toString());
    }
  }
}
