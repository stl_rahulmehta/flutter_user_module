
import 'dart:convert';

class Users{

  final String fullName;

  final String email;

  final String imageUrl;

  final String mobileNumber;

  Users(this.fullName, this.email, this.imageUrl, this.mobileNumber);

  //Get data from server and set in model class
  static List<Users> fromJson(final response) {
    List<Users> users = [];

    var jsonData = jsonDecode(response.body);

    var usersData = jsonData["results"];

    for (var user in usersData) {
      Users newUser = Users(user["name"]["first"] + user["name"]["last"],
          user["email"], user["picture"]["large"], user["phone"]);

      users.add(newUser);
    }

    return users;
  }



  factory Users.fromJson1(Map<String, dynamic> json) {
    return Users(json["name"]["first"] + json["name"]["last"],
        json["email"], json["picture"]["large"], json["phone"]);
  }
}
