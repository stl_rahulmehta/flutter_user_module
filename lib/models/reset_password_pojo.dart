// To parse this JSON data, do
//
//     final resetPasswordPojo = resetPasswordPojoFromJson(jsonString);

import 'dart:convert';

ResetPasswordPojo resetPasswordPojoFromJson(String str) => ResetPasswordPojo.fromJson(json.decode(str));

String resetPasswordPojoToJson(ResetPasswordPojo data) => json.encode(data.toJson());

class ResetPasswordPojo {
  String status;
  String messages;
  Data data;

  ResetPasswordPojo({
    this.status,
    this.messages,
    this.data,
  });

  factory ResetPasswordPojo.fromJson(Map<String, dynamic> json) => ResetPasswordPojo(
    status: json["status"],
    messages: json["messages"],
    data: Data.fromJson(json["data"]),
  );

  Map<String, dynamic> toJson() => {
    "status": status,
    "messages": messages,
    "data": data.toJson(),
  };
}

class Data {
  int id;
  String firstName;
  String lastName;
  String email;
  String mobileNo;
  int otp;
  String profileImage;
  String fileExtension;
  String url;
  DateTime createdAt;
  DateTime updatedAt;

  Data({
    this.id,
    this.firstName,
    this.lastName,
    this.email,
    this.mobileNo,
    this.otp,
    this.profileImage,
    this.fileExtension,
    this.url,
    this.createdAt,
    this.updatedAt,
  });

  factory Data.fromJson(Map<String, dynamic> json) => Data(
    id: json["id"],
    firstName: json["first_name"],
    lastName: json["last_name"],
    email: json["email"],
    mobileNo: json["mobile_no"],
    otp: json["otp"],
    profileImage: json["profile_image"],
    fileExtension: json["file_extension"],
    url: json["url"],
    createdAt: DateTime.parse(json["created_at"]),
    updatedAt: DateTime.parse(json["updated_at"]),
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "first_name": firstName,
    "last_name": lastName,
    "email": email,
    "mobile_no": mobileNo,
    "otp": otp,
    "profile_image": profileImage,
    "file_extension": fileExtension,
    "url": url,
    "created_at": createdAt.toIso8601String(),
    "updated_at": updatedAt.toIso8601String(),
  };
}
