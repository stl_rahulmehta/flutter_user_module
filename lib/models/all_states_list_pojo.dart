// To parse this JSON data, do
//
//     final locationsCommonPojo = locationsCommonPojoFromJson(jsonString);

import 'dart:convert';

LocationsCommonPojo locationsCommonPojoFromJson(String str) =>
    LocationsCommonPojo.fromJson(json.decode(str));

String locationsCommonPojoToJson(LocationsCommonPojo data) =>
    json.encode(data.toJson());

class LocationsCommonPojo {
  String status;
  String messages;
  List<Datum> data;

  LocationsCommonPojo({
    this.status,
    this.messages,
    this.data,
  });

  factory LocationsCommonPojo.fromJson(Map<String, dynamic> json) =>
      LocationsCommonPojo(
        status: json["status"],
        messages: json["messages"],
        data: List<Datum>.from(json["data"].map((x) => Datum.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "status": status,
        "messages": messages,
        "data": List<dynamic>.from(data.map((x) => x.toJson())),
      };
}

class Datum {
  int id;
  String name;
  DateTime createdAt;
  DateTime updatedAt;
  int citiesCount;
  int areasCount;
  int stateId;
  String cityName;
  int cityId;
  String areaName;

  Datum({
    this.id,
    this.name,
    this.createdAt,
    this.updatedAt,
    this.citiesCount,
    this.areasCount,
    this.stateId,
    this.cityName,
    this.cityId,
    this.areaName,
  });

  factory Datum.fromJson(Map<String, dynamic> json) => Datum(
        id: json["id"],
        name: json["name"],
        createdAt: DateTime.parse(json["created_at"]),
        updatedAt: DateTime.parse(json["updated_at"]),
        citiesCount: json["cities_count"],
        areasCount: json["areas_count"],
        stateId: json["state_id"],
        cityName: json["city_name"],
        cityId: json["city_id"],
        areaName: json["area_name"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "name": name,
        "created_at": createdAt.toIso8601String(),
        "updated_at": updatedAt.toIso8601String(),
        "cities_count": citiesCount,
        "areas_count": areasCount,
        "state_id": stateId,
        "city_name": cityName,
        "city_id": cityId,
        "area_name": areaName,
      };
}
